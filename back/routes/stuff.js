const express = require('express');
const router = express.Router();

const stuffCtrl = require('../controllers/stuff');

// ------- POST ---------//
router.post('/', stuffCtrl.createThing);

// ------- PUT ---------//
router.put('/:id', stuffCtrl.modifyThing);

// ------- DELETE ---------//
router.delete('/:id', stuffCtrl.deleteThing);

// ------- GET ---------//
router.get('/:id', stuffCtrl.getOneThing);
router.get('/', stuffCtrl.getAllThing);

module.exports = router;